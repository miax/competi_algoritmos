# Competición de Algoritmos del MIA-X #
## Edición MIA-4 ##

Este repo contiene las bases y la documentación para para participar en la competición del Máster de Inteligencia Artificial
del Instituto BME.  


### Fechas Claves ###

| Fechas        | Descripción  |
| :------------ |:------------ | 
| Feb 28 2020   | Publicación de API y ejemplos |
| Mar 2 2020    | Primer día de acceso. Los participantes pueden enviar o borrar órdenes históricas hasta la fecha actual |
| Mar 31 2020   | Fecha inicio competición. Las órdenes históricas quedarán congeladas, y los participantes podrán enviar sus ordenes sólo en el día desde que estén disponibles los datos hasta las 9:00AM |
| Jul 31 2020   | Fecha final de competición |


### Acceso via API ###
Para descargar los datos y enviar las asignaciones de la cartera de inversión debes conectarte
via API.  Accede a la carpeta 'examples' para ver los ejemplos de comunicación



